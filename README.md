# 公民教育 Civic Education | [civicforum.github.io](https://civicforum.github.io)
培养和提高批判性思考的能力，启发对社会议题的思考

## 为什么要做公民教育?

越多的人具备公民素质，政府就越难欺骗大家，人民才能行使越多本属于自己的权利。

可尽管网上有着天量的优质资源（知识、评论、教程、公开课、新闻流），但大部分人却受制于 __认知泡沫([filter bubble](https://en.wikipedia.org/wiki/Filter_bubble))__ 而无法有效利用这些资源实质性的提高自己--即接触到那些你不知道自己不知道的东西。
本项目就是为了弥补这一缺失的环节。
这个公民教育利用已有的优质资源和平台，引导朋友们对常见且切身的政治论题进行思考、研究、组织和表达，并在于其他参与者互动的过程中理解各种观点、诉求、视角、思维方式、研究技巧，最终既丰富自己的观点，又提高研究问题的水准。

**子曰：“光听课是不行的，自己写一遍作业才能真正掌握它。”**

## 议论题目列表
如果你在墙内，建议通过Tor访问品葱参与讨论，常见应用[Tor Browser](https://www.torproject.org/projects/torbrowser.html) (iPad或iPhone用户可用[Onion Browser](https://blog.torproject.org/tor-heart-onion-browser-and-more-ios-tor))，查看更多[安全上网知识](./online_security.md)

1. [如果人们没有受过民主教育，他们是不应该拥有普选权的](https://pincong.rocks/question/1223)
2. [人权高于主权](https://pincong.rocks/question/1271)
3. [发生重大社会安全事件时，即使认为信息公开会导致骚乱的风险，政府仍然应该开放信息传播](https://pincong.rocks/question/1293)
4. [西方的多党制不适合中国国情](https://pincong.rocks/question/1294)
5. [在中国照搬西方式的言论自由会导致社会失序](https://pincong.rocks/question/1295)
6. [由高校自主考试招生比全国统一考试招生更好](https://pincong.rocks/question/1296)
7. [应该容许宗教人士在非宗教场所公开传教](https://pincong.rocks/question/1297)
8. [中小学生或大学生，都应该参加国家统一安排的军训](https://pincong.rocks/question/1299)
9. [国家的统一和领土完整是社会的最高利益](https://pincong.rocks/question/1300)
10. [哪怕经历了违反程序规定的审讯和取证过程，确实有罪的罪犯也应该被处刑](https://pincong.rocks/question/1291)
11. [国家有义务进行对外援助](https://pincong.rocks/question/1301)
12. [国家领导人及开国领袖的形象可以作为文艺作品的丑化对象](https://pincong.rocks/question/1292)
13. [当法律为能充分制止罪恶行为时，人民群众有权自发对罪恶行为进行制裁](https://pincong.rocks/question/1291)
14. [应当允许媒体代表某一特定阶层或利益集团发言](https://pincong.rocks/question/1302)
15. [如果国家综合实力许可，那么中国有权为了维护自己的利益而采取任何行动](https://pincong.rocks/question/1303)
16. [条件允许的话应该武力统一台湾](https://pincong.rocks/question/1304)
17. [律师即使明知被辩护人的犯罪事实也应当尽力为其进行辩护](https://pincong.rocks/question/1305)
18. [应该允许中国公民同时具有外国国籍](https://pincong.rocks/question/1306)
19. [以美国为首的西方国家不可能真正允许中国崛起成为一流强国](https://pincong.rocks/question/1307)
20. [国家应当采取措施培养和支持体育健儿在各种国际比赛场合为国争光](https://pincong.rocks/question/1308)
21. [最低工资应该由国家规定](https://pincong.rocks/question/1309)
22. [中国改革开放以来的经济发展的成果都被一小群人占有了，大多数人没得到什么好处](https://pincong.rocks/question/1310)
23. [在重大工程羡慕的决策中，个人利益应该为社会利益让路](https://pincong.rocks/question/1311)
24. [浪费粮食也是个人的自由](https://pincong.rocks/question/1312)
25. [如果猪肉价格过高，政府应当干预](https://pincong.rocks/question/1313)
26. [应当对国外同类产品征收高额关税来保护国内民族工业](https://pincong.rocks/question/1314)
27. [教育应当尽量公立](https://pincong.rocks/question/1315)
28. [国有企业的利益属于国家利益](https://pincong.rocks/question/1316)
29. [试图控制房地产价格的行为会破坏经济发展](https://pincong.rocks/question/1317)
30. [改善低收入者生活的首要手段是国家给予财政补贴和扶持](https://pincong.rocks/question/1318)
31. [有钱人理应获得更好的医疗服务](https://pincong.rocks/question/1319)
32. [高收入者应该公开自己的经济来源](https://pincong.rocks/question/1320)
33. [靠运作资金赚钱的人对社会的贡献比不上靠劳动力赚钱的人](https://pincong.rocks/question/1321)
34. [与其让国有企业亏损破产，不如转卖给资本家](https://pincong.rocks/question/1322)
35. [那些关系到国家安全、以及其它重要国计民生的领域，必须全部由国有企业掌控](https://pincong.rocks/question/1323)
36. [资本积累的过程总是伴随着对普通劳动人民利益的伤害](https://pincong.rocks/question/1324)
37. [私人应当可以拥有和买卖土地](https://pincong.rocks/question/1325)
38. [政府应当采取较高的粮食收购价格以增加农民收入](https://pincong.rocks/question/1326)
39. [在华外国资本应享受和民族资本同样的待遇](https://pincong.rocks/question/1208)
40. [市场竞争中自然形成的垄断地位是无害的](https://pincong.rocks/question/1328)
41. [两个成年人之间自愿的性行为是其自由，无论其婚姻关系为何](https://pincong.rocks/question/1329)
42. [不应公开谈论自己长辈的缺点](https://pincong.rocks/question/1330)
43. [现代中国社会需要儒家思想](https://pincong.rocks/question/1331)
44. [判断艺术作品的价值的根本标准是看是不是受到人民大众喜爱](https://pincong.rocks/question/1332)
45. [即使有人口压力，国家和社会也无权干涉个人要不要孩子，要几个孩子](https://pincong.rocks/question/1333)
46. [周易八卦能够有效的解释很多事情](https://pincong.rocks/question/1334)
47. [中国传统医学对人体健康的观念比现代主流医学更高明](https://pincong.rocks/question/1230)
48. [汉字无需人为推行简化](https://pincong.rocks/question/1335)
49. [应当将中国传统文化的经典作品作为儿童基础教育读物](https://pincong.rocks/question/1336)
50. [如果是出于自愿，我会认可我的孩子和同性结成伴侣关系](https://pincong.rocks/question/1337) 


## 相关连接
1. 题目来源:[Jennifer Pan, Yiqing Xu, "China’s Ideological Spectrum"（《中国的意识形态光谱》）](http://jenpan.com/jen_pan/ideology.pdf)
2. 中国政治坐标[测试网站](http://zuobiao.me/), 2014年共有171830位网民参与了测试 [调查数据分析](https://blog.xavierskip.com/2015-05-03-politics-coordinate/)
4. [Center for Civic Education](http://www.civiced.org/)
5. ['Civics Education: Learning About the Rights & Obligations of Citizenship,' NEA.](http://www.nea.org/civicseducation)

## 夹带私货
1. [张博树：中国宪政改革可行性研究报告(全文)](http://minzhuzhongguo.org/sz/report.pdf)
2. [编程随想的博客](https://program-think.blogspot.com/)
3. [一场风的博客](https://yichangfeng.home.blog)
4. 联邦党人文集 [中文版PDF](https://www.gench.edu.cn/_upload/article/e5/bd/536c20404bc4aa8c0aeb3bef50d5/cf27ec85-7636-4841-bb68-1904909d339d.pdf), [英文版在线](http://www.let.rug.nl/usa/documents/1786-1800/the-federalist-papers/), 编程随想推荐 [博客](https://program-think.blogspot.com/2014/01/share-books.html) [Github完整书单](https://github.com/programthink/books) 
5. 可汗学院 [课程：美国历史 US History](https://www.khanacademy.org/humanities/us-history)

更新日期2019/1/13
